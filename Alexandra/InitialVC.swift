//
//  InitialVC.swift
//  Alexandra
//
//  Created by Alexandra Prasolova on 31.07.2020.
//  Copyright © 2020 Alexandra Prasolova. All rights reserved.
//

import UIKit

final class InitialVC: UIViewController {

    /// Set configuration to work with REST API or fake data
    private let dependenciesManager = DependenciesManager(config: .restAPI)
    
    private var feedTab: UINavigationController {
        let feedTab = UIStoryboard.main.instantiateViewController(withIdentifier: "FeedNavController") as! UINavigationController
        let feedVC = feedTab.viewControllers.first! as! FeedVC
        feedVC.dependenciesManager = dependenciesManager
        feedVC.itemsService = dependenciesManager.itemsService
        
        return feedTab
    }
    
    private var profileTab: UINavigationController {
        let profileTab = UIStoryboard.main.instantiateViewController(withIdentifier: "ProfileNavController") as! UINavigationController
        let profileVC = profileTab.viewControllers.first! as! ProfileVC
        profileVC.authService = dependenciesManager.authService
        
        return profileTab
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        performSegue(withIdentifier: "toUITabBarController", sender: nil)
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        switch segue.identifier {
        case "toUITabBarController":
            (segue.destination as! UITabBarController).setViewControllers([feedTab, profileTab], animated: true)
        default:
            break
        }
    }

}
