//
//  ItemsManagerDummy.swift
//  Alexandra
//
//  Created by Alexandra Prasolova on 05.08.2020.
//  Copyright © 2020 Alexandra Prasolova. All rights reserved.
//

import Foundation

final class ItemsManagerDummy: ItemsService {
    
    func fetchItems(page: Int, lastFetchedID: String?, callback:  @escaping ([Item], Error?) -> Void) {
        
        callback([
            Item(id: 200, name: "Nice kitty-cat", imageURL: "https://preview.redd.it/85ade01cxzd51.jpg?width=640&crop=smart&auto=webp&s=5cf4559da053ad4ed8a4c6a6380f77961a6c7e6b", details: "That's one nice kitty-cat right here"),
            Item(id: 100, name: "Party Parrot en un sombrero. Señoras y señores, Buenas tardes, buenas noches", imageURL: "https://preview.redd.it/dr0iq9tb9nd51.jpg?width=640&crop=smart&auto=webp&s=51e711834e5a2e9bee6766a678457fcb0eba22c3", details: "De la Sierra Morena, Cielito lindo, vienen bajando, Un par de ojitos negros, Cielito lindo, de contrabando. \n Ay, ay, ay, ay, Canta y no llores, Porque cantando se alegran, Cielito lindo, los corazones.")], nil)
        
    }
    
}
