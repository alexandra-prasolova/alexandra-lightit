//
//  ItemsManager.swift
//  Alexandra
//
//  Created by Alexandra Prasolova on 30.07.2020.
//  Copyright © 2020 Alexandra Prasolova. All rights reserved.
//

import Foundation


final class ItemsManager: ItemsService {
    
    let network: Network
    
    init(network: Network) {
        self.network = network
    }
    
    func fetchItems(page: Int, lastFetchedID: String?, callback: @escaping ([Item], Error?) -> Void) {
        network.fetchProducts() { result in
            switch result {
                
            case .success(let items):
                callback(items, nil)
            case .failure(let error):
                callback([], error)
            }
        }
    }
    
}
