//
//  ItemsService.swift
//  Alexandra
//
//  Created by Alexandra Prasolova on 05.08.2020.
//  Copyright © 2020 Alexandra Prasolova. All rights reserved.
//

import Foundation

protocol ItemsService: class {
    
    func fetchItems(page: Int, lastFetchedID: String?, callback:  @escaping (_ items: [Item], _ error: Error?) -> Void )
    
}
