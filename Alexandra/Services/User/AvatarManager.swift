//
//  AvatarManager.swift
//  Alexandra
//
//  Created by Alexandra Prasolova on 05.08.2020.
//  Copyright © 2020 Alexandra Prasolova. All rights reserved.
//

import UIKit

final class AvatarManager {
    
    static func deleteAvatar() {
        try? FileManager.default.removeItem(at: avatarURL)
    }
    
    static func fetchAvatar() -> UIImage? {
        let fileURL = avatarURL
        let imageData: Data
        do {
            imageData = try Data(contentsOf: fileURL)
        } catch {
            return nil
        }
        return UIImage(data: imageData)
    }
    
    static func saveAvatar(image: UIImage) {
        let fileURL = avatarURL
        
        if let data = image.jpegData(compressionQuality:  1.0) {
            try? data.write(to: fileURL)
        }
    }
    
    static private var avatarURL: URL {
        let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
        let fileName = "avatar.jpg"
        
        return documentsDirectory.appendingPathComponent(fileName)
    }
    
}
