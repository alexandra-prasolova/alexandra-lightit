//
//  AuuthService.swift
//  Alexandra
//
//  Created by Alexandra Prasolova on 03.08.2020.
//  Copyright © 2020 Alexandra Prasolova. All rights reserved.
//

import Foundation

protocol AuthService {
    func register(username: String, password: String, callback: @escaping (Error?) -> Void)
    func login(username: String, password: String, callback: @escaping (Error?) -> Void)
    func logout(callback: @escaping (Error?) -> Void)
    func fillProfile(firstName: String?, secondName: String?)
    
    var currentUser: User? { get }
}
