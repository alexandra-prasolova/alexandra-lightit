//
//  AuthManager.swift
//  Alexandra
//
//  Created by Alexandra Prasolova on 05.08.2020.
//  Copyright © 2020 Alexandra Prasolova. All rights reserved.
//

import Foundation

final class AuthManager: AuthService {
    
    let network: Network
    let keychain: Keychain
    let profilePlist = Plist<String>(name: "Profile")
    
    private(set) var currentUser: User?
    
    init(network: Network, keychain: Keychain) {
        self.network = network
        self.keychain = keychain
        loadUserFromPlist()
        keychain.username = currentUser?.username
    }
    
    func register(username: String, password: String, callback: @escaping (Error?) -> Void) {
        network.register(username: username, password: password) { result in
            switch result {
            case .success(let token):
                self.saveUserToPlist(username: username)
                self.keychain.username = username
                self.currentUser = User(id: 13, username: username)
                let status = self.keychain.saveToKeychain(token)
                guard status == errSecSuccess else {
                    callback(KeychainError(status: status))
                    return
                }
                callback(nil)
            case .failure(let error):
                callback(error)
            }
        }
    }
    
    func login(username: String, password: String, callback: @escaping (Error?) -> Void) {
        network.login(username: username, password: password) { result in
            switch result {
            case .success(let token):
                self.saveUserToPlist(username: username)
                self.keychain.username = username
                self.currentUser = User(id: 13, username: username)
                let status = self.keychain.saveToKeychain(token)
                guard status == errSecSuccess else {
                    callback(KeychainError(status: status))
                    return
                }
                callback(nil)
            case .failure(let error):
                callback(error)
            }
        }
    }
    
    func logout(callback: @escaping (Error?) -> Void) {
        deleteLocalUserData()
        currentUser = nil
        network.logout() { [weak self] result in
            guard let self = self else { return }
            switch result {
            case .success:
                callback(nil)
            case .failure(let error):
                callback(error)
            }
            self.keychain.deleteAllFromKeychain()
            self.keychain.username = nil
        }
        AvatarManager.deleteAvatar()
    }
    
    func fillProfile(firstName: String?, secondName: String?) {
        guard currentUser != nil else { return }
        
        currentUser?.firstName = firstName
        currentUser?.secondName = secondName
        
        saveUserToPlist(username: currentUser!.username, firstName: firstName, secondName: secondName)
    }
    
    private func saveUserToPlist(username: String, firstName: String? = nil, secondName: String? = nil) {
        var userData: [String: String] = ["username": username]
        if let firstName = firstName {
            userData["firstName"] = firstName
        }
        if let secondName = secondName {
            userData["secondName"] = secondName
        }
        try? profilePlist?.setValuesToPlistFile(dictionary: userData)
    }
    
    private func deleteLocalUserData() {
        try? self.profilePlist?.setValuesToPlistFile(dictionary: [:])
    }
    
    private func loadUserFromPlist() {
        guard let usrData = profilePlist?.getValuesInPlistFile(), !usrData.isEmpty else { return }
        guard let username: String = usrData["username"] else { return }
        guard let firstName: String = usrData["firstName"] else { return }
        guard let secondName: String = usrData["secondName"] else { return }
        let user = User(id: 13, username: username, firstName: firstName, secondName: secondName)
        
        currentUser = user
    }
}
