//
//  AuthServiceDummy.swift
//  Alexandra
//
//  Created by Alexandra Prasolova on 05.08.2020.
//  Copyright © 2020 Alexandra Prasolova. All rights reserved.
//

import Foundation

final class AuthServiceDummy: AuthService {
    
    private(set) var currentUser: User?
    
    func register(username: String, password: String, callback: @escaping (Error?) -> Void) {
        currentUser = User(id: 13, username: username)
        callback(nil)
    }
    
    func login(username: String, password: String, callback: @escaping (Error?) -> Void) {
        currentUser = User(id: 13, username: username)
        callback(nil)
    }
    
    func logout(callback: @escaping (Error?) -> Void) {
        currentUser = nil
        callback(nil)
    }
    
    func fillProfile(firstName: String?, secondName: String?) {
        guard currentUser != nil else { return }
        
        currentUser?.firstName = firstName
        currentUser?.secondName = secondName
    }
    
}
