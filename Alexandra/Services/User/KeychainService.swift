//
//  KeychainService.swift
//  Alexandra
//
//  Created by Alexandra Prasolova on 04.08.2020.
//  Copyright © 2020 Alexandra Prasolova. All rights reserved.
//

import Foundation

public struct KeychainError: Error, LocalizedError {

    public var status: OSStatus?

    public init(status: OSStatus?) {
        self.status = status
    }
    
    public var errorDescription: String? {
        return status?.description ?? "Unknown keychain error"
    }
}

final class Keychain {
    
    var username: String?
    
    func saveToKeychain(_ token: String) -> OSStatus? {
        guard let username = username else { return nil }
        let tag = self.tag(username: username)
        let keyData = token.data(using: .utf8)!
        let query: [String: Any] = [kSecClass as String: kSecClassKey,
                                       kSecAttrApplicationTag as String: tag,
                                       kSecValueData as String: keyData]
        _ = SecItemDelete(query as CFDictionary)
        let status = SecItemAdd(query as CFDictionary, nil)
        
        return status
    }
    
    func getTokenFromKeychain() -> String? {
        guard let username = username else { return nil }
        let tag = self.tag(username: username)
        
        let getquery: [String: Any] = [kSecClass as String: kSecClassKey,
                                       kSecAttrApplicationTag as String: tag,
                                       kSecReturnData as String: true]
        
        var item: CFTypeRef?
        SecItemCopyMatching(getquery as CFDictionary, &item)
       
        guard let keyData = item as? Data else { return nil }
        let value = String(data: keyData, encoding: .utf8)
        return value
    }
    
    func deleteAllFromKeychain() {
        let secItemClasses = [kSecClassGenericPassword, kSecClassInternetPassword, kSecClassCertificate, kSecClassKey, kSecClassIdentity]
        for itemClass in secItemClasses {
            let spec: NSDictionary = [kSecClass: itemClass]
            SecItemDelete(spec)
        }
    }
    
    private func tag(username: String) -> Data {
        let tag = "com.alexandra.keys.token.\(username)".data(using: .utf8)!
        return tag
    }
    
}
