//
//  Plist.swift
//  Alexandra
//
//  Created by Alexandra Prasolova on 04.08.2020.
//  Copyright © 2020 Alexandra Prasolova. All rights reserved.
//

import Foundation

struct Plist<T> {
    
    enum PlistError: Error {
        case FileNotWritten
        case FileDoesNotExist
    }
    
    let name: String
    
    var sourcePath: String? {
        guard let path = Bundle.main.path(forResource: name, ofType: "plist") else { return nil }
        return path
    }
    
    var destPath: String? {
        let dir = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
        return (dir as NSString).appendingPathComponent("\(name).plist")
    }
    
    init?(name:String) {
        self.name = name
        
        let fileManager = FileManager.default
        guard let destination = destPath else { return nil }
        
        if !fileManager.fileExists(atPath: destination) {
            
            fileManager.createFile(atPath: destination, contents: nil, attributes: [.type: "plist"])
            
        }
        guard fileManager.fileExists(atPath: destination) else { return nil }
    }
    
    
    func getValuesInPlistFile() -> Dictionary<String, T> {
        let fileManager = FileManager.default
        if fileManager.fileExists(atPath: destPath!) {
            guard let dict = NSDictionary(contentsOfFile: destPath!) else { return [:]}
            return dict as? Dictionary<String, T> ?? [:]
        } else {
            return [:]
        }
    }
    
    /// Rewrites whole plist with this argument dictionary
    func setValuesToPlistFile(dictionary: Dictionary<String, T>) throws {
        let fileManager = FileManager.default
        if fileManager.fileExists(atPath: destPath!) {
            if !(dictionary as NSDictionary).write(toFile: destPath!, atomically: false) {
                
                throw PlistError.FileNotWritten
            }
        } else {
            throw PlistError.FileDoesNotExist
        }
    }
    
    func deleteValue(name: String) throws {
        let fileManager = FileManager.default
        if fileManager.fileExists(atPath: destPath!) {
            guard let dict = NSDictionary(contentsOfFile: destPath!) else { return }
            dict.setValue(nil, forKey: name)
            if !dict.write(toFile: destPath!, atomically: false) {
                
                throw PlistError.FileNotWritten
            }
        } else {
            throw PlistError.FileDoesNotExist
        }
    }
}

