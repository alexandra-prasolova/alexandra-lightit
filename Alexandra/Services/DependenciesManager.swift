//
//  DependenciesManager.swift
//  Alexandra
//
//  Created by Alexandra Prasolova on 31.07.2020.
//  Copyright © 2020 Alexandra Prasolova. All rights reserved.
//

import Foundation

final class DependenciesManager {
    
    enum Config {
        case fakes
        case restAPI
    }
    
    private(set) var config: Config = .fakes
    private var network: Network
    private var keychain = Keychain()
    
    private(set) lazy var itemsService: ItemsService = config == .fakes ? ItemsManagerDummy() : ItemsManager(network: network)
    private(set) lazy var reviewsService: ReviewsService = config == .fakes ? ReviewsManagerDummy() : ReviewsManager(network: network)
    private(set) lazy var authService: AuthService = config == .fakes ? AuthServiceDummy() : AuthManager(network: network, keychain: keychain)
    
    init(config: Config) {
        self.config = config
        self.network = Network(keychain: keychain)
    }
    
}
