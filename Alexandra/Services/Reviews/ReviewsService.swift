//
//  ReviewsService.swift
//  Alexandra
//
//  Created by Alexandra Prasolova on 05.08.2020.
//  Copyright © 2020 Alexandra Prasolova. All rights reserved.
//

import Foundation

protocol ReviewsService {
    
    func fetchReviewsForItem(itemID: Int, callback: @escaping ([Review], _ error: Error?) -> Void)
    func leaveReview(text: String, rate: Int, user: User, itemId: Int, callback: @escaping ((Review, _ error: Error?) -> Void))
    
}
