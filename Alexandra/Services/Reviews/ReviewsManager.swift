//
//  ReviewsManager.swift
//  Alexandra
//
//  Created by Alexandra Prasolova on 31.07.2020.
//  Copyright © 2020 Alexandra Prasolova. All rights reserved.
//

import Foundation

final class ReviewsManager: ReviewsService {
    
    let network: Network
    
    init(network: Network) {
        self.network = network
    }
    
    func fetchReviewsForItem(itemID: Int, callback: @escaping ([Review], Error?) -> Void) {
        network.fetchReviews(itemId: itemID) { result in
            switch result {
            case .success(let reviews):
                callback(reviews, nil)
            case .failure(let error):
                callback([], error)
            }
        }
    }
    
    func leaveReview(text: String, rate: Int, user: User, itemId: Int, callback: @escaping ((Review, Error?) -> Void)) {
        network.postReview(itemId: itemId, rate: rate, text: text) { result in
            var review = Review(id: 0, rate: rate, text: text, user: user, itemId: itemId)
            switch result {
            case .success(let reviewId):
                review.id = reviewId
                callback(review, nil)
            case .failure(let error):
                callback(review, error)
            }
        }
    }
    
}
