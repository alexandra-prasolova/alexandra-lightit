//
//  ReviewsManagerDummy.swift
//  Alexandra
//
//  Created by Alexandra Prasolova on 05.08.2020.
//  Copyright © 2020 Alexandra Prasolova. All rights reserved.
//

import Foundation

final class ReviewsManagerDummy: ReviewsService {
    
    private var reviews = [Review]()
    
    init() {
        reviews = [Review(id: 1, rate: 4, text: "Kinda OK", user: User(id: 10, username: "contentuser"), itemId: 13),
                   Review(id: 2, rate: 5, text: "Niiice", user: User(id: 11, username: "happyuser"), itemId: 13),
                   Review(id: 3, rate: 1, text: "It sucks. If I could rate it 0 I totally would. In fact, I wish I could rate it -100500. 0/10 don't recommend.", user: User(id: 12, username: "angryuser"), itemId: 13)]
    }
    
    func fetchReviewsForItem(itemID: Int, callback: @escaping ([Review], Error?) -> Void) {
        callback(reviews, nil)
    }
    
    func leaveReview(text: String, rate: Int, user: User, itemId: Int, callback: @escaping ((Review, Error?) -> Void)) {
        let review = Review(id: 0, rate: rate, text: text, user: user, itemId: itemId)
        reviews.append(review)
        callback(review, nil)
    }
    
}
