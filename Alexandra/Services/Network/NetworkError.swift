//
//  NetworkError.swift
//  Alexandra
//
//  Created by Alexandra Prasolova on 04.08.2020.
//  Copyright © 2020 Alexandra Prasolova. All rights reserved.
//

import Foundation

public enum NetworkError: Error, LocalizedError {
    
    case undefinedMapperForType(Any.Type)
    case undefinedRequestGeneratorForType(Any.Type)
    case unexpectedResponse
    case unknownError
    case message(String)
    case cancelled
    case unauthorized
    case notFound
    case badGateway
    
    init?(errorCode: Int) {
        switch errorCode {
        case 401:
            self = .unauthorized
        case 404:
            self = .notFound
        case 502:
            self = .badGateway
        default:
            return nil
        }
    }
    
    public var errorDescription: String? {
        switch self {
        case .unknownError:
            return "Unknown network error."
        case .unauthorized:
            return "Unauthorized."
        case .undefinedMapperForType(let type):
            return "Undefined network mapper for type `\(type)`."
        case .undefinedRequestGeneratorForType(let type):
            return "Undefined network request generator for type `\(type)`."
        case .unexpectedResponse:
            return "Unexpected network response."
        case .message(let message):
            return "\(message)"
        case .cancelled:
            return "Network task is cancelled."
        case .badGateway:
            return "Bad Gateway"
        case .notFound:
            return "Network resource not found."
        }
    }
}
