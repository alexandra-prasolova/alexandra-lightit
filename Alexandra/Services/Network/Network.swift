//
//  Network.swift
//  Alexandra
//
//  Created by Alexandra Prasolova on 04.08.2020.
//  Copyright © 2020 Alexandra Prasolova. All rights reserved.
//

import Foundation
import Security

public typealias NetworkResponse = (data: Data?, meta: URLResponse?, error: Error?)

enum HTTPHeaderField: String {
    case authentication = "Authorization"
    case contentType = "Content-Type"
}

enum ContentType: String {
    case json = "application/json"
    case token = "Token "
}

final class Network {
    
    enum HTTPMethod {
        static let get = "GET"
        static let post = "POST"
        static let patch = "PATCH"
        static let put = "PUT"
        static let delete = "DELETE"
    }
    
    private let mapper = Mapper()
    let keychain: Keychain!
    private let api = "http://smktesting.herokuapp.com/api"
    
    private var session: URLSession {
        let config = URLSessionConfiguration.default
        config.timeoutIntervalForRequest = 30
        config.timeoutIntervalForResource = 60
        let session = URLSession(configuration: config)
        return session
    }
    
    init(keychain: Keychain) {
        self.keychain = keychain
    }
    
    // MARK: Auth
    
    func login(username: String, password: String, callback: @escaping (Result<String, Error>) -> Void) {
        var request = URLRequest(url: URL(string: api + "/login/")!)
        request.httpMethod = HTTPMethod.post
        let params: [String: Any] =  ["username": username, "password": password]
        request.addValue(ContentType.json.rawValue, forHTTPHeaderField: HTTPHeaderField.contentType.rawValue)
        request.httpBody = try? httpBody(from: params)
        session.dataTask(with: request) { (data, response, error) in
            do {
                let data = try self.handle((data, response, error))
                let token: String = try self.mapper.mapToToken(data)
                DispatchQueue.main.async {
                    callback(.success(token))
                }
            } catch let error {
                DispatchQueue.main.async {
                    callback(.failure(error))
                }
            }
        }.resume()
    }
    
    func register(username: String, password: String, callback: @escaping (Result<String, Error>) -> Void) {
        var request = URLRequest(url: URL(string: api + "/register/")!)
        request.httpMethod = HTTPMethod.post
        request.addValue(ContentType.json.rawValue, forHTTPHeaderField: HTTPHeaderField.contentType.rawValue)
        request.httpBody = try? httpBody(from: ["username": username, "password": password])
        session.dataTask(with: request) { (data, response, error) in
            do {
                let data = try self.handle((data, response, error))
                let token: String = try self.mapper.mapToToken(data)
                DispatchQueue.main.async {
                    callback(.success(token))
                }
            } catch let error {
                DispatchQueue.main.async {
                    callback(.failure(error))
                }
            }
        }.resume()
    }
    
    func logout(callback: @escaping (Result<Void, Error>) -> Void) {
        guard let token = token() else {
            DispatchQueue.main.async {
                callback(.failure(NetworkError.unauthorized ))
            }
            return
        }
        
        var request = URLRequest(url: URL(string: api + "/logout/")!)
        request.httpMethod = HTTPMethod.post
        request.addValue(ContentType.json.rawValue, forHTTPHeaderField: HTTPHeaderField.contentType.rawValue)
        request.addValue(ContentType.token.rawValue + token, forHTTPHeaderField: HTTPHeaderField.authentication.rawValue)
        
        session.dataTask(with: request) { (data, response, error) in
            do {
                try self.handle((data, response, error))
                DispatchQueue.main.async {
                    callback(.success(Void()))
                }
            } catch let error {
                DispatchQueue.main.async {
                    callback(.failure(error))
                }
            }
        }.resume()
    }
    
    // MARK: Items
    
    func fetchProducts(callback: @escaping (Result<[Item], Error>) -> Void) {
        var request = URLRequest(url: URL(string: api + "/products")!)
        request.httpMethod = HTTPMethod.get
        session.dataTask(with: request) { (data, response, error) in
            do {
                let data = try self.handle((data, response, error))
                let entity: [Item] = try self.mapper.mapToList(data)
                DispatchQueue.main.async {
                    callback(.success(entity))
                }
            } catch let error {
                DispatchQueue.main.async {
                    callback(.failure(error))
                }
            }
        }.resume()
    }
    
    // MARK: Reviews
    
    func fetchReviews(itemId: Int, callback: @escaping (Result<[Review], Error>) -> Void) {
        var request = URLRequest(url: URL(string: api + "/reviews/\(itemId)")!)
        request.httpMethod = HTTPMethod.get
        session.dataTask(with: request) { (data, response, error) in
            do {
                let data = try self.handle((data, response, error))
                let entity: [Review] = try self.mapper.mapToList(data)
                DispatchQueue.main.async {
                    callback(.success(entity))
                }
            } catch let error {
                DispatchQueue.main.async {
                    callback(.failure(error))
                }
            }
        }.resume()
    }
    
    func postReview(itemId: Int, rate: Int, text: String, callback: @escaping (Result<Int, Error>) -> Void) {
        guard let token = token() else {
            DispatchQueue.main.async {
                callback(.failure(NetworkError.unauthorized ))
            }
            return
        }
        var request = URLRequest(url: URL(string: api + "/reviews/\(itemId)")!)
        request.httpMethod = HTTPMethod.post
        request.addValue(ContentType.json.rawValue, forHTTPHeaderField: HTTPHeaderField.contentType.rawValue)
        request.addValue(ContentType.token.rawValue + token, forHTTPHeaderField: HTTPHeaderField.authentication.rawValue)
        request.httpBody = try? httpBody(from: ["rate": rate,
                                                "text": text])
        session.dataTask(with: request) { (data, response, error) in
            do {
                let data = try self.handle((data, response, error))
                let entity: Int = (try? self.mapper.mapToId(data)) ?? 42
                DispatchQueue.main.async {
                    callback(.success(entity))
                }
            } catch let error {
                DispatchQueue.main.async {
                    callback(.failure(error))
                }
            }
        }.resume()
    }
    
    // MARK: private
    
    @discardableResult private func handle(_ response: NetworkResponse) throws -> Data {
        do {
            
            if let error = response.error {
                if (error as NSError).code == NSURLErrorCancelled {
                    throw NetworkError.cancelled
                } else {
                    throw error
                }
            }
            
            guard let data = response.data, let meta = response.meta as? HTTPURLResponse else {
                throw NetworkError.unknownError
            }
            
            let isErrorCode = meta.statusCode >= 400
            
            if !isErrorCode {
                return data
            }
            
            if let error = NetworkError(errorCode: meta.statusCode) {
                throw error
            }
            
            if let message = String(bytes: data, encoding: .utf8) {
                throw NetworkError.message(message)
            }
            
            throw NetworkError.unknownError
            
        } catch let error {
            throw error
        }
    }
    
    private func httpBody(from dictionary: Dictionary<String, Any>) throws -> Data {
        let dict = dictionary
        do {
            let data = try JSONSerialization.data(withJSONObject: dict)
            return data
        } catch {
            throw error
        }
    }
    
    private func token() -> String? {
        return keychain.getTokenFromKeychain()
    }
    
}
