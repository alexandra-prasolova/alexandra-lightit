//
//  Mapper.swift
//  Alexandra
//
//  Created by Alexandra Prasolova on 04.08.2020.
//  Copyright © 2020 Alexandra Prasolova. All rights reserved.
//

import Foundation

final class Mapper {
    
    func mapToEntity<Entity>(_ dictionary: [String: Any]) throws -> Entity {
        switch Entity.self {
        case is Item.Type:
            return try mapToItem(dictionary) as! Entity
        case is Review.Type:
            return try mapToReview(dictionary) as! Entity
        default:
            throw NetworkError.undefinedMapperForType(Entity.self)
        }
    }
    
    func mapToList<Entity>(_ dictionary: [[String: Any]]) throws -> [Entity] {
        return try dictionary.map(mapToEntity)
    }
    
    func mapToEntity<Entity>(_ data: Data) throws -> Entity {
        let response: Any
        do {
            response = try JSONSerialization.jsonObject(with: data, options: [])
        } catch {
            throw error
        }
        if let dict = response as? [String: Any] {
            return try mapToEntity(dict)
        } else {
            throw NetworkError.unexpectedResponse
        }
    }
    
    func mapToList<Entity>(_ data: Data) throws -> [Entity] {
        let response: Any
        do {
            response = try JSONSerialization.jsonObject(with: data, options: [])
        } catch {
            throw error
        }
        if let dict = response as? [[String: Any]] {
            return try mapToList(dict)
        } else {
            throw NetworkError.unexpectedResponse
        }
    }
    
    // MARK: Item
    
    func mapToItem(_ dictionary: [String: Any]) throws -> Item {
        
        let id = dictionary["id"] as! Int
        let name = dictionary["title"] as! String
        let text = dictionary["text"] as! String
        let imageName = dictionary["img"] as! String
        
        let entity = Item(id: id, name: name, imageURL: String.imageURL(name: imageName), details: text)
        
        return entity
    }
    
    //MARK: Review
    
    func mapToReview(_ dictionary: [String: Any]) throws -> Review {
        
        let id = dictionary["id"] as! Int
        let itemId = dictionary["product"] as! Int
        let rate = dictionary["rate"] as! Int
        let text = dictionary["text"] as! String
        let user: User
        do {
            user = try mapToUser(dictionary["created_by"] as! [String : Any])
        } catch {
            throw error
        }
           
        let entity = Review(id: id, rate: rate, text: text, user: user, itemId: itemId)
           
        return entity
    }
    
    //MARK: User
    
    func mapToUser(_ dictionary: [String: Any]) throws -> User {
        let id = dictionary["id"] as! Int
        let name = dictionary["username"] as! String
        
        return User(id: id, username: name)
    }
    
    // MARK: Token
    func mapToToken(_ data: Data) throws -> String {
        let response: Any
        do {
            response = try JSONSerialization.jsonObject(with: data, options: [])
        } catch {
            throw error
        }
        guard let dict = response as? [String: Any] else { throw NetworkError.unexpectedResponse }
        if let token = dict["token"] as? String {
            return token
        } else if let message = dict["message"] as? String {
            throw NetworkError.message(message)
        } else { throw NetworkError.unexpectedResponse }
    }
    
    // MARK: Review ID
    func mapToId(_ data: Data) throws -> Int {
        let response: Any
        do {
            response = try JSONSerialization.jsonObject(with: data, options: [])
        } catch {
            throw error
        }
        if let dict = response as? [String: Any], let id = dict["review_id"] as? Int {
            return id
        } else {
            throw NetworkError.unexpectedResponse
        }
    }
    
}
