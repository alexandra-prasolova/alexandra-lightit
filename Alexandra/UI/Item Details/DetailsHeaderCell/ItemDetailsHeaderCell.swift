//
//  ItemDetailsHeaderCell.swift
//  Alexandra
//
//  Created by Alexandra Prasolova on 02.08.2020.
//  Copyright © 2020 Alexandra Prasolova. All rights reserved.
//

import UIKit

final class ItemDetailsHeaderCell: UITableViewCell {

    @IBOutlet private var titleLabel: UILabel!
    @IBOutlet private var descriptionLabel: UILabel!
    @IBOutlet private var itemImageView: UIImageView!
    
    private weak var imageRatioConstraint: NSLayoutConstraint?
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        itemImageView.image = nil
        descriptionLabel.text = ""
        titleLabel.text = ""
        
        if let constraint = imageRatioConstraint {
            itemImageView.removeConstraint(constraint)
            imageRatioConstraint = nil
        }
    }
    
    func set(title: String, image: UIImage?, details: String) {
        titleLabel.text = title
        descriptionLabel.text = details
        
        if let img = image {
            let ratio = NSLayoutConstraint(item: self.itemImageView!, attribute: .width, relatedBy: .equal, toItem: self.itemImageView, attribute: .height, multiplier: img.size.width/img.size.height, constant: 0)
            self.itemImageView.addConstraint(ratio)
            self.imageRatioConstraint = ratio
        }
        itemImageView.image = image
    }
    
}
