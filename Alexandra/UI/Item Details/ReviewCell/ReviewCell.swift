//
//  ReviewCell.swift
//  Alexandra
//
//  Created by Alexandra Prasolova on 02.08.2020.
//  Copyright © 2020 Alexandra Prasolova. All rights reserved.
//

import UIKit

final class ReviewCell: UITableViewCell {
    
    @IBOutlet private var reviewLabel: UILabel!
    @IBOutlet private var authorLabel: UILabel!
    @IBOutlet private var rateViewContainer: UIView!
    
    private let rateView = RateView.loadFromNib()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        rateViewContainer.addSubview(rateView)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        rateView.frame = rateViewContainer.bounds
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        reviewLabel.text = ""
        authorLabel.text = ""
    }
    
    func set(rate: Int, text: String, author: String) {
        rateView.rate = rate
        reviewLabel.text = text
        authorLabel.text = "By " + author
    }
}
