//
//  LeaveReviewVC.swift
//  Alexandra
//
//  Created by Alexandra Prasolova on 03.08.2020.
//  Copyright © 2020 Alexandra Prasolova. All rights reserved.
//

import UIKit

final class LeaveReviewVC: KeyboardAppearanceHandlerViewController {
    
    var model: ItemDetailsModel!
    
    @IBOutlet private var textView: UITextView!
    @IBOutlet private var rateViewContainer: UIView!
    private var rateView = RateView.loadFromNib()
    
    @IBOutlet private var actionButtonBottomConstraint: NSLayoutConstraint!
       
    override var constraintStickingToKeyboard: NSLayoutConstraint? {
        return actionButtonBottomConstraint
    }
    
    override var uppercaseConstant: CGFloat {
        return 16
    }
    
    override var lowercaseConstant: CGFloat {
        return 16
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        rateView.editable = true
        rateViewContainer.addSubview(rateView)
        startWatchingTapOutideTextInput()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        startKeyboardTracking()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        textView.becomeFirstResponder()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        stopKeyboardTracking()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        rateView.frame = rateViewContainer.bounds
    }
    
    @IBAction private func reviewButtonTapped (_ sender: Any) {
        
        let text = textView.text ?? ""
        let rate = rateView.rate
        
        model.review(text: text, rate: rate) { [weak self] error in
            if let errorText = error?.localizedDescription {
                self?.showError(errorText: errorText)
            } else {
                self?.dismiss(animated: true, completion: nil)
            }
        }
    }

    @IBAction private func cancelButtonTapped(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
}
