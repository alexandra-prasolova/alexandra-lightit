//
//  ReviewsHeaderView.swift
//  Alexandra
//
//  Created by Alexandra Prasolova on 03.08.2020.
//  Copyright © 2020 Alexandra Prasolova. All rights reserved.
//

import UIKit

final class ReviewsHeaderView: UIView {
    
    private var label = UILabel()
    
    private let inset: CGFloat = 16
    
    var text: String {
        set {
            label.text = newValue
        }
        get {
            return label.text ?? ""
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        label.font = UIFont.systemFont(ofSize: UIFont.smallSystemFontSize)
        backgroundColor = .systemBackground
        label.backgroundColor = .clear
        addSubview(label)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        label.frame = CGRect(x: inset, y: 0, width: bounds.width - inset*2, height: bounds.height)
    }
    
}
