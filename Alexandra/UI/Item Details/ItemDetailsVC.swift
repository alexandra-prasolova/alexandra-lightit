//
//  ItemDetailsVC.swift
//  Alexandra
//
//  Created by Alexandra Prasolova on 01.08.2020.
//  Copyright © 2020 Alexandra Prasolova. All rights reserved.
//

import UIKit

final class ItemDetailsVC: UIViewController {
    
    var model: ItemDetailsModel!
    var dependenciesManager: DependenciesManager!
    
    @IBOutlet private var reviewsTableView: UITableView!
    @IBOutlet private var bottomView: UIView!
    @IBOutlet private var authView: UIView!
    @IBOutlet private var leaveReviewButton: UIButton!
    
    private var itemImage: UIImage?
    
    private var refreshing = false
    private var refreshControl = UIRefreshControl()
    
    private var reviews: [Review] {
        return model.reviews
    }
    
    private var item: Item {
        return model.item
    }
    
    private let reviewsHeaderHeight: CGFloat = 32
    private let sectionFooterHeight: CGFloat = 8
    private let headerSection = 0
    private let reviewsSection = 1
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        model.delegate = self
        refresh()
        
        navigationItem.title = "Item details"
        
        reviewsTableView.register(ItemDetailsHeaderCell.self)
        reviewsTableView.register(ReviewCell.self)
        reviewsTableView.rowHeight = UITableView.automaticDimension
        reviewsTableView.estimatedRowHeight = 300
        
        reviewsTableView.refreshControl = refreshControl
        refreshControl.addTarget(self, action: #selector(refresh), for: .valueChanged)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        configure()
    }
    
    private func configure() {
        leaveReviewButton.isHidden = !model.canReview
        authView.isHidden = model.canReview
    }
    
    @objc private func refresh() {
        guard !refreshing else { return }
        refreshControl.endRefreshing()
        refreshing = true
        model.fetchRevies() { [weak self] error in
            self?.refreshing = false
            if let errorText = error?.localizedDescription {
                DispatchQueue.main.asyncAfter(deadline: .now()+0.5) {
                    self?.showError(errorText: errorText)
                }
            }
        }
    }
    
    @IBAction private func leaveReviewTapped(_ sender: Any) {
        performSegue(withIdentifier: "toLeaveReviewVC", sender: nil)
    }
    
    @IBAction private func registerTapped(_ sender: Any) {
        performSegue(withIdentifier: "toRegisterFromDetails", sender: nil)
    }
    
    @IBAction private func loginTapped(_ sender: Any) {
        performSegue(withIdentifier: "toLoginFromDetails", sender: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        switch segue.identifier {
        case "toLeaveReviewVC":
            guard let reviewVC = segue.destination as? LeaveReviewVC else { return }
            reviewVC.model = model
        case "toLoginFromDetails":
            let authVC = segue.destination as! AuthenticationVC
            authVC.authService = dependenciesManager.authService
            authVC.scenario = .login
        case "toRegisterFromDetails":
            let authVC = segue.destination as! AuthenticationVC
            authVC.authService = dependenciesManager.authService
            authVC.scenario = .register
        default:
            break
        }
    }
    
}

extension ItemDetailsVC: ItemDetailsModelDelegate {
    
    func itemDetailsModel(_ itemDetailsModel: ItemDetailsModel, didReceiveReviews reviews: [Review]) {
        reviewsTableView.reloadSections([reviewsSection], with: .automatic)
    }
    
    func itemDetailsModel(_ itemDetailsModel: ItemDetailsModel, didReceiveImage image: UIImage) {
        self.itemImage = image
        reviewsTableView.reloadSections([headerSection], with: .automatic)
    }
}

extension ItemDetailsVC: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        guard section == 1 else { return nil }
        let reviewsHeader = ReviewsHeaderView(frame: CGRect(x: 0, y: 0, width: 100, height: reviewsHeaderHeight))
        reviewsHeader.text = "\(reviews.count) reviews"
        
        return reviewsHeader
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let footer = UIView(frame: CGRect(x: 0, y: 0, width: 100, height: sectionFooterHeight))
        footer.backgroundColor = .clear
        return footer
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return section == reviewsSection ? bottomView.frame.height : sectionFooterHeight
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return section == reviewsSection ? reviewsHeaderHeight : 0
    }
}

extension ItemDetailsVC: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case headerSection:
            return 1
        case reviewsSection:
            return reviews.count
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case headerSection:
            let cell = tableView.dequeue(at: indexPath, cell: ItemDetailsHeaderCell.self)
            cell.set(title: model.item.name, image: itemImage, details: model.item.details)
            return cell
        case reviewsSection:
            let cell = tableView.dequeue(at: indexPath, cell: ReviewCell.self)
            let review = reviews[indexPath.row]
            cell.set(rate: review.rate, text: review.text, author: review.user.username)
            return cell
        default:
            return UITableViewCell()
        }
    }
    
}
