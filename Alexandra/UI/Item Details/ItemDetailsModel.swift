//
//  ItemDetailsModel.swift
//  Alexandra
//
//  Created by Alexandra Prasolova on 02.08.2020.
//  Copyright © 2020 Alexandra Prasolova. All rights reserved.
//

import UIKit

protocol ItemDetailsModelDelegate: class {
    
    func itemDetailsModel(_ itemDetailsModel: ItemDetailsModel, didReceiveReviews reviews: [Review])
    func itemDetailsModel(_ itemDetailsModel: ItemDetailsModel, didReceiveImage image: UIImage)
    
}

final class ItemDetailsModel {
    
    weak var delegate: ItemDetailsModelDelegate?
    
    private(set) var item: Item
    private(set) var reviews = [Review]()
    
    private var reviewsService: ReviewsService
    private var authService: AuthService
    
    var canReview: Bool {
        return authService.currentUser != nil
    }
    
    init(item: Item, reviewsService: ReviewsService, authService: AuthService) {
        self.item = item
        self.reviewsService = reviewsService
        self.authService = authService
        
        if let url = URL(string: item.imageURL) {
            downloadImage(by: url) { image in
                self.delegate?.itemDetailsModel(self, didReceiveImage: image)
            }
        }
    }
    
    func fetchRevies(callback: ((Error?) -> Void)?) {
        reviewsService.fetchReviewsForItem(itemID: item.id) { reviews, error in
            if let err = error {
                callback?(err)
            } else {
                self.reviews = reviews
                self.delegate?.itemDetailsModel(self, didReceiveReviews: reviews)
                callback?(nil)
            }
        }
    }
    
    func review(text: String, rate: Int, callback: ((Error?) -> Void)?) {
        guard canReview else { return }
        let user = authService.currentUser!
        reviewsService.leaveReview(text: text, rate: rate, user: user, itemId: item.id) { [weak self] review, error in
            guard let self = self else { return }
            if let err = error {
                callback?(err)
            } else {
                self.reviews.append(review)
                self.delegate?.itemDetailsModel(self, didReceiveReviews: self.reviews)
                callback?(nil)
            }
        }
    }
    
}
