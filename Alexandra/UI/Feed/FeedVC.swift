//
//  ViewController.swift
//  Alexandra
//
//  Created by Alexandra Prasolova on 29.07.2020.
//  Copyright © 2020 Alexandra Prasolova. All rights reserved.
//

import UIKit

final class FeedVC: UIViewController {
    
    var itemsService: ItemsService!
    var dependenciesManager: DependenciesManager!
    
    @IBOutlet private var tableView: UITableView!
    @IBOutlet private var emptyStateLabel: UILabel!
    @IBOutlet private var activityIndicator: UIActivityIndicatorView!
    
    private var selectedIndex: Int?
    
    private var refreshing = false
    private var refreshControl = UIRefreshControl()
    
    private var items: [Item] = [] {
        didSet {
            emptyStateLabel.isHidden = !items.isEmpty
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.title = "All Products"
        
        tableView.register(FeedTableViewCell.self)
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 380
        
        tableView.refreshControl = refreshControl
        refreshControl.addTarget(self, action: #selector(refresh), for: .valueChanged)
        
        activityIndicator.startAnimating()
        items = []
        refresh()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        switch segue.identifier {
        case "toItemDetailsVC":
            guard let detailsVC = segue.destination as? ItemDetailsVC, let i = selectedIndex else { return }
            detailsVC.dependenciesManager = dependenciesManager
            detailsVC.model = ItemDetailsModel(item: items[i], reviewsService: dependenciesManager.reviewsService, authService: dependenciesManager.authService)
            selectedIndex = nil
        default:
            break
        }
    }
    
    @objc func refresh() {
        refreshControl.endRefreshing()
        
        guard !refreshing else { return }
        refreshing = true
        itemsService.fetchItems(page: 0, lastFetchedID: nil) { [weak self] items, error in
            self?.activityIndicator.stopAnimating()
            self?.refreshing = false
            if let err = error {
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                    self?.showError(errorText: err.localizedDescription)
                }
            } else {
                self?.items = items
                self?.tableView.reloadData()
            }
        }
    }

}

extension FeedVC: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedIndex = indexPath.row
        performSegue(withIdentifier: "toItemDetailsVC", sender: nil)
    }
}

extension FeedVC: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeue(at: indexPath, cell: FeedTableViewCell.self)
        let item = items[indexPath.row]
        cell.set(title: item.name, imageURL: item.imageURL)
        return cell
    }
    
}

