//
//  AuthenticationVC.swift
//  Alexandra
//
//  Created by Alexandra Prasolova on 03.08.2020.
//  Copyright © 2020 Alexandra Prasolova. All rights reserved.
//

import UIKit

enum AuthError: Error, LocalizedError {
    case repeatedPass
    case emptyUsername
    case emptyPass
    
    var errorDescription: String {
        switch self {
        case .emptyPass:
            return "Password may not be empty"
        case .emptyUsername:
            return "Username may not be empty"
        case .repeatedPass:
            return "Passwords don't match"
        }
    }
}

final class AuthenticationVC: KeyboardAppearanceHandlerViewController {
    
    enum Scenario {
        case login
        case register
    }
    
    var scenario: Scenario = .login
    var authService: AuthService!
    
    @IBOutlet private var actionButton: UIButton!
    @IBOutlet private var usernameTextField: UITextField!
    @IBOutlet private var passwordTextField: UITextField!
    @IBOutlet private var repeatPasswordTextField: UITextField!
    @IBOutlet private var actionButtonBottomConstraint: NSLayoutConstraint!
    
    override var constraintStickingToKeyboard: NSLayoutConstraint? {
        return actionButtonBottomConstraint
    }
    
    override var uppercaseConstant: CGFloat {
        return 16
    }
    
    override var lowercaseConstant: CGFloat {
        return 16
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        switch scenario {
        case .login:
            actionButton.setTitle("Login", for: .normal)
            repeatPasswordTextField.isHidden = true
            navigationItem.title = "Login"
        case .register:
            actionButton.setTitle("Register", for: .normal)
            repeatPasswordTextField.isHidden = false
            navigationItem.title = "New User"
        }
        
        startWatchingTapOutideTextInput()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        startKeyboardTracking()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        stopKeyboardTracking()
    }
    
    @IBAction private func actionButtonTapped(_ sender: Any) {
        let username = usernameTextField.text ?? ""
        let password = passwordTextField.text ?? ""
        let repeatedPassword = repeatPasswordTextField.text ?? ""
        
        let callback: (Error?) -> Void = { [weak self] error in
            if let errorText = error?.localizedDescription {
                self?.showError(errorText: errorText)
            } else {
                self?.showMessage(messageText: nil, title: "Success") {
                    self?.navigationController?.popViewController(animated: true)
                }
            }
        }
        
        switch scenario {
        case .login:
            let valid = validateForLogin(username: username, password: password)
            if valid {
                authService.login(username: username, password: password, callback: callback)
            }
        case .register:
            let valid = validateForRegistration(username: username, password: password, repeatedPassword: repeatedPassword)
            if valid {
                authService.register(username: username, password: password, callback: callback)
            }
        }
    }
    
    private func validateForLogin(username: String, password: String) -> Bool {
        var result = true
        if username.isEmpty {
            showError(errorText: AuthError.emptyUsername.localizedDescription)
            result = false
        } else if password.isEmpty {
            showError(errorText: AuthError.emptyPass.localizedDescription)
            result = false
        }
        return result
    }
    
    private func validateForRegistration(username: String, password: String, repeatedPassword: String) -> Bool {
        var result = true
        
        if username.isEmpty {
            showError(errorText: AuthError.emptyUsername.localizedDescription)
            result = false
        } else if password.isEmpty {
            showError(errorText: AuthError.emptyPass.localizedDescription)
            result = false
        } else if password != repeatedPassword {
            showError(errorText: AuthError.repeatedPass.localizedDescription)
            result = false
        }
        return result
        
    }
    
}
