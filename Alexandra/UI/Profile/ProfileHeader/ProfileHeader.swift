//
//  ProfileHeader.swift
//  Alexandra
//
//  Created by Alexandra Prasolova on 05.08.2020.
//  Copyright © 2020 Alexandra Prasolova. All rights reserved.
//

import UIKit

final class ProfileHeader: UIView, NibLoadable {
    
    @IBOutlet private var imageView: UIImageView!
    
    func set(_ image: UIImage?) {
        imageView.image = image ?? UIImage(systemName: "person")
    }
}
