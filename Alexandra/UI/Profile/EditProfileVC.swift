//
//  EditProfileVC.swift
//  Alexandra
//
//  Created by Alexandra Prasolova on 04.08.2020.
//  Copyright © 2020 Alexandra Prasolova. All rights reserved.
//

import UIKit

final class EditProfileVC: KeyboardAppearanceHandlerViewController {
    
    var authService: AuthService!
    
    @IBOutlet private var avatarImageView: UIImageView!
    @IBOutlet private var firstNameTextField: UITextField!
    @IBOutlet private var secondNameTextField: UITextField!
    @IBOutlet private var bottomConstraint: NSLayoutConstraint!
    @IBOutlet private var addAvatarButton: UIButton!
    @IBOutlet private var crossImageView: UIImageView!
    
    @IBOutlet private var emptyStateView: UIView!
    
    override var lowercaseConstant: CGFloat {
        return 0
    }
    
    override var uppercaseConstant: CGFloat {
        return 0
    }
    
    override var constraintStickingToKeyboard: NSLayoutConstraint? {
        return bottomConstraint
    }
    
    private var user: User! {
        return authService.currentUser
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        guard authService.currentUser != nil else {
            emptyStateView.isHidden = false
            return
        }

        navigationItem.title = user.username
        
        secondNameTextField.text = user.secondName
        firstNameTextField.text = user.firstName
        
        avatarImageView.image = AvatarManager.fetchAvatar()
        crossImageView.isHidden = avatarImageView.image == nil
        
        startWatchingTapOutideTextInput()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        startKeyboardTracking()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        stopKeyboardTracking()
    }
    
    @IBAction private func deleteAvatarTapped() {
        avatarImageView.image = nil
        AvatarManager.deleteAvatar()
        crossImageView.isHidden = true
    }
    
    @IBAction private func addAvatarTapped() {
        let imagePickerVC = UIImagePickerController()
        imagePickerVC.sourceType = .photoLibrary
        imagePickerVC.delegate = self
        present(imagePickerVC, animated: true)
    }
    
    @IBAction private func saveTapped() {
        let firstName = firstNameTextField.text ?? ""
        let secondName = secondNameTextField.text ?? ""
        
        authService.fillProfile(firstName: firstName.isEmpty ? nil : firstName, secondName: secondName.isEmpty ? nil : secondName)
        if let avatar = avatarImageView.image {
            AvatarManager.saveAvatar(image: avatar)
        }
        
        navigationController?.popViewController(animated: true)
    }
    
}

extension EditProfileVC: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        picker.dismiss(animated: true)

        guard let image = info[.originalImage] as? UIImage else { return }
        avatarImageView.image = image
        crossImageView.isHidden = false
    }
    
}
