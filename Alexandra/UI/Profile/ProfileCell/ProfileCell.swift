//
//  ProfileCell.swift
//  Alexandra
//
//  Created by Alexandra Prasolova on 03.08.2020.
//  Copyright © 2020 Alexandra Prasolova. All rights reserved.
//

import UIKit

final class ProfileCell: UITableViewCell {
    
    @IBOutlet private var leftLabel: UILabel!
    @IBOutlet private var rightLabel: UILabel!

    func set(leftText: String, rightText: String) {
        leftLabel.text = leftText
        rightLabel.text = rightText
    }
    
}
