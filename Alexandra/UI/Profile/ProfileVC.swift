//
//  ProfileVC.swift
//  Alexandra
//
//  Created by Alexandra Prasolova on 03.08.2020.
//  Copyright © 2020 Alexandra Prasolova. All rights reserved.
//

import UIKit

final class ProfileVC: UIViewController {
    
    struct ProfileCellData {
        var field: String
        var text: String
    }
    
    var authService: AuthService!
    
    @IBOutlet private var tableView: UITableView!
    @IBOutlet private var logoutButton: UIButton!
    
    @IBOutlet private var unathorizedStateView: UIView!
    
    private var profileData: [ProfileCellData] = []
    private var avatar: UIImage?
    
    private var user: User? {
        return authService.currentUser
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.register(ProfileCell.self)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        configureForState(authorized: user != nil)
        configureProfileData(for: user)
        avatar = AvatarManager.fetchAvatar()
    }
    
    private func configureForState(authorized: Bool) {
        unathorizedStateView.isHidden = authorized
        logoutButton.isHidden = !authorized
    }
    
    private func configureProfileData(for user: User?) {
        profileData = []
        guard let usr = user else {
            tableView.reloadData()
            return
        }
        profileData.append(ProfileCellData(field: "Username", text: usr.username))
        profileData.append(ProfileCellData(field: "First Name", text: usr.firstName ?? ""))
        profileData.append(ProfileCellData(field: "Second Name", text: usr.secondName ?? ""))
        tableView.reloadData()
    }
    
    @IBAction private func logoutTapped(_ sender: Any) {
        authService.logout() { [weak self] error in
            guard let self = self else { return }
            if let errorText = error?.localizedDescription {
                self.showError(errorText: errorText)
            }
            self.configureForState(authorized: self.user != nil)
            self.configureProfileData(for: self.user)
            
        }
    }
    
    @IBAction private func registerTapped(_ sender: Any) {
        performSegue(withIdentifier: "toRegister", sender: nil)
    }
    
    @IBAction private func loginTapped(_ sender: Any) {
        performSegue(withIdentifier: "toLogin", sender: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        switch segue.identifier {
        case "toRegister":
            guard let authVC = segue.destination as? AuthenticationVC else { return }
            authVC.authService = authService
            authVC.scenario = .register
        case "toLogin":
            guard let authVC = segue.destination as? AuthenticationVC else { return }
            authVC.authService = authService
            authVC.scenario = .login
        case "toEditProfileVC":
            guard let editProfileVC = segue.destination as? EditProfileVC else { return }
            editProfileVC.authService = authService
        default:
            break
        }
    }
    
}

extension ProfileVC: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 44
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        guard user != nil else { return nil }
        let header = ProfileHeader.loadFromNib()
        header.set(avatar)
        return header
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        guard user != nil else { return 0 }
        return tableView.frame.width/2
    }
}

extension ProfileVC: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return profileData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeue(at: indexPath, cell: ProfileCell.self)
        let data = profileData[indexPath.row]
        
        cell.set(leftText: data.field, rightText: data.text)
        return cell
    }
}
