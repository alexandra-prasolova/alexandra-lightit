//
//  RateView.swift
//  Alexandra
//
//  Created by Alexandra Prasolova on 02.08.2020.
//  Copyright © 2020 Alexandra Prasolova. All rights reserved.
//

import UIKit

final class RateView: UIView, NibLoadable {
    
    var editable = false
    var starsCount = 5
    
    @IBOutlet private var collectionView: UICollectionView!
    
    var rate = 1 {
        didSet {
            collectionView.reloadData()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        collectionView.register(RateViewCollectionViewCell.self)
    }
    
}

extension RateView: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: bounds.height, height: bounds.height)
    }
}

extension RateView: UICollectionViewDelegate {}

extension RateView: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return starsCount
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeue(at: indexPath, cell: RateViewCollectionViewCell.self)
        cell.set(filled: indexPath.item + 1 <= rate)
        cell.tapAction = { [weak self] in
            guard let self = self, self.editable else { return }
            self.rate = indexPath.item + 1
        }
        return cell
    }
}
