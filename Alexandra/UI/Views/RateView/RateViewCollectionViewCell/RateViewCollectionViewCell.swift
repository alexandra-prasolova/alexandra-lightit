//
//  RateViewCollectionViewCell.swift
//  Alexandra
//
//  Created by Alexandra Prasolova on 02.08.2020.
//  Copyright © 2020 Alexandra Prasolova. All rights reserved.
//

import UIKit

final class RateViewCollectionViewCell: UICollectionViewCell {

    @IBOutlet private var starImageView: UIImageView!
    
    var tapAction: NoArgumentsBlock?
    
    func set(filled: Bool) {
        starImageView.image = filled ? UIImage(systemName: "star.fill") : UIImage(systemName: "star")
    }
    
    @IBAction private func cellTapped() {
        tapAction?()
    }
    
}
