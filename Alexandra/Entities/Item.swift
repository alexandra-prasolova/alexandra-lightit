//
//  Item.swift
//  Alexandra
//
//  Created by Alexandra Prasolova on 30.07.2020.
//  Copyright © 2020 Alexandra Prasolova. All rights reserved.
//

import Foundation

/*
 • Id—идентификаторпродукта
 • Title—наименованиепродукта
 • Image—изображениепродукта
 • Text—описаниепродукта
 */

public struct Item {
    var id: Int
    var name: String
    var imageURL: String
    var details: String
}
