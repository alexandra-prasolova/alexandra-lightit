//
//  User.swift
//  Alexandra
//
//  Created by Alexandra Prasolova on 03.08.2020.
//  Copyright © 2020 Alexandra Prasolova. All rights reserved.
//

import Foundation

struct User {
    var id: Int
    var username: String
    var firstName: String?
    var secondName: String?
    
    init(id: Int, username: String, firstName: String? = nil, secondName: String? = nil) {
        self.id = id
        self.username = username
        self.firstName = firstName
        self.secondName = secondName
    }
}
