//
//  Review.swift
//  Alexandra
//
//  Created by Alexandra Prasolova on 30.07.2020.
//  Copyright © 2020 Alexandra Prasolova. All rights reserved.
//

import Foundation

/*
 • Id—идентификаторотзыва
 • Rate—оценка
 • Text—комментарий
 • Id_user—идентификаторпользователя
 • Id_entry—идентификаторпродукта

 */

public struct Review {
    var id: Int
    var rate: Int
    var text: String
    var user: User
    var itemId: Int
}
