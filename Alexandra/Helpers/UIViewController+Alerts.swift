//
//  UIViewController+Alerts.swift
//  Alexandra
//
//  Created by Alexandra Prasolova on 31.07.2020.
//  Copyright © 2020 Alexandra Prasolova. All rights reserved.
//

import UIKit

public extension UIViewController {
    
    func showError(errorText: String) {
        let alertController = UIAlertController(title: NSLocalizedString("Error", comment: ""), message: errorText, preferredStyle: .alert)
        
        let cancelAction = UIAlertAction(title: "OK", style: .default)
        alertController.addAction(cancelAction)
        
        present(alertController, animated: true, completion: nil)
    }
    
    func showMessage(messageText: String?, title: String?, completion: NoArgumentsBlock? = nil) {
        let alertController = UIAlertController(title: title, message: messageText, preferredStyle: .alert)
        
        let cancelAction = UIAlertAction(title: "OK", style: .default) { _ in
            completion?()
        }
        alertController.addAction(cancelAction)
        
        present(alertController, animated: true, completion: nil)
    }
    
}
