//
//  String+Image URL.swift
//  Alexandra
//
//  Created by Alexandra Prasolova on 04.08.2020.
//  Copyright © 2020 Alexandra Prasolova. All rights reserved.
//

import Foundation

extension String {
    static func imageURL(name: String) -> String {
        return "http://smktesting.herokuapp.com/static/" + name
    }
}
