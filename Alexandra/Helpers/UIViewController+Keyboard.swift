//
//  UIViewController+Keyboard.swift
//  Alexandra
//
//  Created by Alexandra Prasolova on 03.08.2020.
//  Copyright © 2020 Alexandra Prasolova. All rights reserved.
//

import UIKit

class KeyboardAppearanceHandlerViewController: UIViewController {
    
    var constraintStickingToKeyboard: NSLayoutConstraint? {
        return nil
    }
    var uppercaseConstant: CGFloat {
        return 0
    }
    var lowercaseConstant: CGFloat {
        return 0
    }
    
    func startKeyboardTracking() {
        let center: NotificationCenter = .default
        
        center.addObserver(self, selector: #selector(willShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        center.addObserver(self, selector: #selector(willHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    func stopKeyboardTracking() {
        let center: NotificationCenter = .default
        center.removeObserver(self)
        center.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
        center.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    func keyboardWillShow(in rect: CGRect, animationDuration: TimeInterval) {
        let stickingConstraintConstant = rect.height - view.safeAreaInsets.bottom
        
        constraintStickingToKeyboard?.constant = stickingConstraintConstant + uppercaseConstant
        
        UIView.animate(withDuration: animationDuration, animations: {
            self.view.layoutIfNeeded()
        })
    }
    
    func keyboardWillHide(in rect: CGRect, animationDuration: TimeInterval) {
        constraintStickingToKeyboard?.constant = lowercaseConstant
        UIView.animate(withDuration: animationDuration, animations: {
            self.view.layoutIfNeeded()
        })
    }
    
     @objc private func willHide(_ notification: Notification) {
        guard let userInfo = notification.userInfo else { return }
        
        let rect = userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? CGRect ?? .zero
        let duration = userInfo[UIResponder.keyboardAnimationDurationUserInfoKey] as? Double ?? 0
        
        self.keyboardWillHide(in: rect, animationDuration: duration)
    }
    
    @objc private func willShow(_ notification: Notification) {
        guard let userInfo = notification.userInfo else { return }
        
        let rect = userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? CGRect ?? .zero
        let duration = userInfo[UIResponder.keyboardAnimationDurationUserInfoKey] as? Double ?? 0
        
        self.keyboardWillShow(in: rect, animationDuration: duration)
    }
    
}

extension UIViewController {
    
    func startWatchingTapOutideTextInput() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(tapView))
        view.addGestureRecognizer(tap)
    }
    
    @objc
    private func tapView(_ sender: UIGestureRecognizer) {
        if !(sender.view is UITextInput) {
            view.endEditing(true)
        }
    }
}
