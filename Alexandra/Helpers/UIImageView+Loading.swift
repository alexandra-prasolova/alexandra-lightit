//
//  UIImage+Loading.swift
//  Alexandra
//
//  Created by Alexandra Prasolova on 31.07.2020.
//  Copyright © 2020 Alexandra Prasolova. All rights reserved.
//

import UIKit

extension UIImageView {
    
    func loadImage(by url: URL) {
        let indicator = UIActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: 37, height: 37))
        indicator.center = center
        indicator.style = .large
        addSubview(indicator)
        indicator.startAnimating()
        downloadImage(by: url) { [weak self] image in
            indicator.stopAnimating()
            indicator.removeFromSuperview()
            self?.image = image
        }
    }
}

public func downloadImage(by url: URL, completion: @escaping ((UIImage) -> Void)) {
    URLSession.shared.dataTask(with: url) { data, response, error in
        guard
            let httpURLResponse = response as? HTTPURLResponse, httpURLResponse.statusCode == 200,
            let mimeType = response?.mimeType, mimeType.hasPrefix("image"),
            let data = data, error == nil,
            let image = UIImage(data: data)
            else { return }
        DispatchQueue.main.async() {
            completion(image)
        }
    }.resume()
}
